#include <iostream> 
using namespace std; 
  
int main() 
{ 
    int size; 


    cout << "Введите количество элементов в массиве: "; 
    cin >> size;
    int numbers[size]; 
    int max_number;
    for (int i = 0; i < size; i++) { 
        cout << "Введите число: "; 
        cin >> numbers[i]; 
        if (i==0) {
            max_number = numbers[0];
        } else {
            if (numbers[i] > max_number) {
                max_number = numbers[i];
            }
        }
    }
  
    cout << "Элементы массива: "; 
    for (int i = 0; i < size; i++) { 
        cout << numbers[i];
        if (i < size - 1) {
            cout << ", ";
        }
    } 
    cout << endl;

    cout << "Наибольший элемент: " << max_number << endl; 

  
    return 0; 
}